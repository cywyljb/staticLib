package com.james.library;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import net.grandcentrix.tray.AppPreferences;
import net.grandcentrix.tray.BuildConfig;

import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yourname
 * @date 2023/8/8 10:56
 */
public class StaticUtil {

    public static void dealAmStart(Intent intent, Context context){
        AppPreferences appPreferences = new AppPreferences(context);
        String tenvideo2 = intent.getStringExtra("tenvideo2");//腾讯视频用的
        String area = intent.getStringExtra("area");//统计位置参数用的
        if (TextUtils.isEmpty(tenvideo2)) {
            Log.i("---tenvideo2--@----", "tenvideo2 为空");
        } else {
            Log.i("---tenvideo2--@----", tenvideo2);
            appPreferences.put("tenvideo2", tenvideo2);
            appPreferences.put("detailFlag", true);
        }
        if (TextUtils.isEmpty(area)) {
            Log.i("---area--@----", "area 为空");
        } else {
            Log.i("---area--@----", area);
            appPreferences.put("area", area);
        }
    }

    public static String statistics(String url,String keyNo,String regionCode, String vipCode,String orderState, String vipName, String pageUrl, String pageName, String visitTime, String pageTitle, String versionName) throws Exception {

        //添加参数
        Map<String, String> data = new HashMap<String, String>();
        data.put("keyNo", keyNo);
        data.put("branchNo", regionCode);
        data.put("vipCode", vipCode);
        data.put("orderState", orderState);
        data.put("vipName", URLBuilder.encodeURIComponent(vipName));
        data.put("pageUrl", URLBuilder.encodeURIComponent(pageUrl));
        data.put("pageName", URLBuilder.encodeURIComponent(pageName));
        data.put("visitTime", visitTime + "");//System.currentTimeMillis()
        data.put("pageTitle", URLBuilder.encodeURIComponent(pageTitle));
        data.put("versionName", versionName);

        showText("-------------:data body :" + data.toString());
        Connection connection = Jsoup.connect(url)
//                .ignoreContentType(true)
//                .userAgent("Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36") // User-Agent of Chrome 55
//                    .referrer("http://bluetata.com/")
//                .header("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")//application/json; charset=UTF-8
//                .header("Accept", "text/plain, */*; q=0.01")
//                .header("Accept-Encoding", "gzip,deflate,sdch")
//                .header("Accept-Language", "es-ES,es;q=0.8")
//                .header("Connection", "keep-alive")
//                .header("X-Requested-With", "XMLHttpRequest")
//                .requestBody(jsonObject.toString())
                .maxBodySize(0)
                .timeout(1000 * 100);
//                .method(Connection.Method.POST);


        //获取响应
        Connection.Response response = connection.data(data).method(Connection.Method.POST).ignoreContentType(true).execute();

        //获取数据，处理成HTML
        Document document = response.parse();

        showText("-------------statistics请求到的数据:response body :" + document.body().text());

        return document.body().text();
    }

    private static String TAG = "StaticUtil";
    public static void showText(String str) {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, str);
        }
        Log.e(TAG, str);
    }
    
}
